import _ from 'underscore'
import Backbone from 'backbone'

const UserItemView = Backbone.View.extend({
    tagName:'li',
    className: 'list-group-item',

    template: _.template(`<%= name %>`),
    
    initialize(){
        console.log('UserView', this.el)
        this.listenTo(this.model,'remove', this.remove )
    },

    render(){
        this.$el.html(this.template(this.model.toJSON()))
    }
})

export default UserItemView;