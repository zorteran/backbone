import _ from 'underscore'
import Backbone from 'backbone'
import UsersItemView from './user-item.view'

        // var item = new UserItemView({
        //     el:this.el,
        //     model: this.users.first()
        // });

const UsersListView = Backbone.View.extend({
    template: _.template(`<div>
        <ul class="list-group">         
        </ul>
    </div>`),
    
    initialize(){
        // this.listenTo(this.collection, 'reset', this.render)
        this.listenTo(this.collection, 'remove', this.remove)
        this.listenTo(this.collection, 'add', this.add)
    },
    containerSelector: '.list-group',
    items: [],

    add(model) { 
        var view = new UsersItemView({
            model
        }); 
        this.items.push(view)
        view.render()
        this.container.append(view.el)
    },

    remove(model) {
        var view =  _.findWhere(this.items,{model});
        view.remove();
    },

    render(){
        if(!this.container){
            this.$el.html(this.template())
            this.container = this.$(this.containerSelector)
        }
        this.collection.each( this.add.bind(this) )
    }
})

export default UsersListView;