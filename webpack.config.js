const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path')

module.exports = {
    entry:{
        main: './src/main.js'
    },
    output:{
        path: path.join(__dirname,'dist'),
        // filename: '[name]-[hash].js'
        filename: 'bundle.js'
    },
    devtool: 'source-map',
    module:{
        rules:[
            {test: /\.js$/, use:'babel-loader', exclude:/node_modules/ },
            {test: /\.css$/, use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader']
                })
            }
        ]
    },
    plugins:[
        new ExtractTextPlugin('styles.css'),
        new HtmlWebpackPlugin({
            template: './src/index.html',
        }),
        // new HtmlWebpackPlugin({
        //     template: './src/index.html',
        //     filename:'admin.html',
        // })
    ]
}